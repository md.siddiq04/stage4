package com.cognizant.truyum;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.cognizant.truyum.model.MenuItem.menuitem;
@Repository
public interface MenuItemRepository extends CrudRepository<menuitem, Integer> {

}
