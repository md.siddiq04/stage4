package com.cognizant.truyum.service.MenuItemService;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.truyum.dao.MenuItemDao.MenuItemDao;
import com.cognizant.truyum.dao.MenuItemDaoCollectionImpl.MenuItemdaoCollecionImpl;
//import com.cognizant.truyum.dao.MenuItemDaoCollectionImpl.MenuItemdaoCollecionImpl;
import com.cognizant.truyum.model.MenuItem.menuitem;

@Service
public class MenuItemService {
	@Autowired
	MenuItemDao menuitemdao;
	
	public List<menuitem> getMenuItemListCustomer(){
		System.out.println("service");
		return menuitemdao.getMenuItemListCustomer();
	}
	
	public menuitem getMenuItem(int menuId) {
		return menuitemdao.getMenuItem(menuId);
		
	}
	public void modifyMenuItem(menuitem menuit) {
		menuitemdao.modifyMenuItem(menuit);
		
	}

}
