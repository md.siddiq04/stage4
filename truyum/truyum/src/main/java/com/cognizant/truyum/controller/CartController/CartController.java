package com.cognizant.truyum.controller.CartController;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.truyum.model.MenuItem.menuitem;
import com.cognizant.truyum.service.CartService.CartService;

@RestController
@RequestMapping("/carts")
public class CartController {
	@Autowired
	private CartService cartService;
	
		@PostMapping("/{userId}/{menuItemId}")
	     public void addCartItem(@PathVariable("userId") String userId, @PathVariable("menuItemId") int menuItemId) {
		
		cartService.addCartItem(userId, menuItemId);

	}
		
		
		@GetMapping("/{userId}")
		public ArrayList<menuitem> getAllCartItems(@PathVariable("userID") String userId) {
					return cartService.getAllCartItems(userId);
		}
		
		@DeleteMapping("/{userId}/{menuItemId}")
		public void deleteCartItems(@PathVariable("userId") String userId, @PathVariable("menuItemId") int menuItemId) {
			cartService.deleteCartItem(userId, menuItemId);
		}

}
