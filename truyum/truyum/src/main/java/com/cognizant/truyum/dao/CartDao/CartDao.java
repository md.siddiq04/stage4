package com.cognizant.truyum.dao.CartDao;

import java.util.ArrayList;

import com.cognizant.truyum.model.MenuItem.menuitem;

public interface CartDao {
	void addCartItem(String userId, int menuItemId);
	ArrayList<menuitem> getAllCartItems(String userId);

	void deleteCartItem(String userId, int menuItemId);

}
