package com.cognizant.springlearn.HelloCntroller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.springlearn.Country;
import com.cognizant.springlearn.CountryService.CountryService;
import com.cognizant.springlearn.CountryService.Exception.CountryNotFoundException;

@RestController
public class HelloController {
	@GetMapping("/hello")
	public String sayHello() {
		return "hello World";
	}
	

}
