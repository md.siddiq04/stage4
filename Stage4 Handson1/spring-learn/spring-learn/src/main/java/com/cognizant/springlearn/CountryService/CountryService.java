package com.cognizant.springlearn.CountryService;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.cognizant.springlearn.Country;
import com.cognizant.springlearn.SpringLearnApplication;
import com.cognizant.springlearn.CountryService.Exception.CountryNotFoundException;

public class CountryService {
	private static final Logger LOGGER = LoggerFactory.getLogger(SpringLearnApplication.class);
	
	public List<Country> getCountry(String code) throws CountryNotFoundException {
		{
			List<Country> list = new ArrayList<>() ;
			ApplicationContext context = new ClassPathXmlApplicationContext("country.xml");
			List<Country> country =  (List<Country>)context.getBean("countryList", ArrayList.class);
			//country.get(0).g
			for(int i = 0 ;i<country.size();i++) {
				if(country.get(i).getCode().equalsIgnoreCase(code)) {
					list.add(country.get(i)) ;
					
				}
			}
			if(list.isEmpty()) {
				throw new CountryNotFoundException();
			}else {
			return list;
			}
		}
	}

	public List<Country> deleteCountry(String code) {
		// TODO Auto-generated method stub
		LOGGER.info("STart delete");
		ApplicationContext context = new ClassPathXmlApplicationContext("country.xml");
		List<Country> country =  (List<Country>)context.getBean("countryList", ArrayList.class);
		LOGGER.info("STart middl");
		for(int i = 0 ;i<country.size();i++) {
			LOGGER.info("STart for");
			if(country.get(i).getCode().equals(code)) {
				LOGGER.info("STart if");
				country.remove(i);
				LOGGER.info("if end ");
				break;
			}
		}
		
		LOGGER.info("end delete");
		return country;
		
		
		
		
	}

	public List<Country> updateCountry(String code) {
		// TODO Auto-generated method stub
		return null;
	} 
		
		
	

}
