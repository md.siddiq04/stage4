package com.cognizant.loans;

public class Loan {
	private String loanNumber;
	private String loanType;
	public String getLoanNumber() {
		return loanNumber;
	}
	public void setLoanNumber(String loanNumber) {
		this.loanNumber = loanNumber;
	}
	public String getLoanType() {
		return loanType;
	}
	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}
	public int getLoanAmount() {
		return loanAmount;
	}
	public void setLoanAmount(int loanAmount) {
		this.loanAmount = loanAmount;
	}
	public int getLoanEmi() {
		return loanEmi;
	}
	public void setLoanEmi(int loanEmi) {
		this.loanEmi = loanEmi;
	}
	public int getLoanTenure() {
		return loanTenure;
	}
	public void setLoanTenure(int loanTenure) {
		this.loanTenure = loanTenure;
	}
	private int loanAmount;
	private int loanEmi;
	private int loanTenure;
	

}
